using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TradingMaster.Application.DTO;
using TradingMaster.Application.Interfaces;
using TradingMaster.Core.Entities;
using TradingMaster.Infrastructure;

namespace TradingMaster.Application.Services
{
    public class WalletService : IWalletService
    {
        private readonly TradingMasterContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WalletService(
            UserManager<User> userManager,
            TradingMasterContext dbContext,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<Wallet>> GetUserWalletAsync()
        {
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            if (user == null) throw new Exception("User not found");

            var userId = user.Id;
            try
            {
                var userWallets = await _dbContext.Wallets.Where(w => w.UserId == userId).ToListAsync();
                return userWallets;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error while retrieving user wallet", ex);
            }
        }

        public async Task<Wallet> CreateUserWalletAsync(AppWalletRegistration request)
        {
            var user = new User();
            if (request.UserId != null)
            {
                user = await _userManager.FindByIdAsync(request.UserId);
            }
            else
            {
                user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            }
            
            if (user == null) throw new Exception("User not found");

            var userId = user.Id;

            var wallet = new Wallet { Balance = request.Balance, UserId = userId, User = user };

            try {
                _dbContext.Wallets.Add(wallet);
                await _dbContext.SaveChangesAsync();
                return wallet;
            } catch (Exception ex) {
                throw new ApplicationException("Error while creating user wallet", ex);
            }
        }

        public async Task<Wallet> AddBalanceToWalletByWalletId(AppWalletAddBalance request)
        {
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            if (user == null) throw new Exception("User not found");

            var userId = user.Id;
            var wallet = await _dbContext.Wallets.FindAsync(request.WalletId);
            if (wallet == null || wallet.UserId != userId)
                throw new Exception("Wallet not found or does not belong to the specified user.");

            wallet.Balance += request.Income;
            try
            {
                _dbContext.Wallets.Update(wallet);
                await _dbContext.SaveChangesAsync();

                return wallet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error while adding balance to wallet", ex);
            }
        }

        public async Task<Wallet> WithdrawBalanceFromWalletByWalletId(AppWalletWithdrawBalance request)
        {
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            if (user == null) throw new Exception("User not found");

            var userId = user.Id;
            var wallet = await _dbContext.Wallets.FindAsync(request.WalletId);
            if (wallet == null || wallet.UserId != userId)
                throw new Exception("Wallet not found or does not belong to the specified user.");

            if (wallet.Balance < request.Withdrawal) throw new Exception("Insufficient funds");
            wallet.Balance -= request.Withdrawal;
            try
            {
                _dbContext.Wallets.Update(wallet);
                await _dbContext.SaveChangesAsync();

                return wallet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error while withdrawing balance from wallet", ex);
            }
        }
    }
}
using System.Text.Json;
using TradingMaster.Application.DTO;
using TradingMaster.Application.Interfaces;

namespace TradingMaster.Application.Services;

public class MarketService : IMarketService
{
    private readonly HttpClient _httpClient;

    public MarketService(HttpClient httpClient)
    {
        _httpClient = httpClient;
        _httpClient.DefaultRequestHeaders.Add(
            "X-CoinAPI-Key",
            "43BD366D-07D5-43EE-AFFB-09516FD10CCA"
        );
    }

    public async Task<IEnumerable<AppAsset>> GetAssetsAsync(string? assetId)
    {
        var url = "https://rest.coinapi.io/v1/assets";
        if (assetId != null)
        {
            url += $"?filter_asset_id={assetId}";
        }

        var response = await _httpClient.GetAsync(url);
        var responseBody = await response.Content.ReadAsStringAsync();
        var assets = JsonSerializer.Deserialize<IEnumerable<AppAsset>>(responseBody);
        // take only the first 10 assets
        return assets.Take(20);
    }

    public async Task<IEnumerable<AppSymbol>> GetSymbolsAsync(string? assetId)
    {
        assetId ??= "ETH";
        var response = await _httpClient.GetAsync($"https://rest.coinapi.io/v1/symbols?filter_symbol_id={assetId}");
        response.EnsureSuccessStatusCode();

        var responseBody = await response.Content.ReadAsStringAsync();
        var symbols = JsonSerializer.Deserialize<IEnumerable<AppSymbol>>(responseBody);
        // take only the first 10 symbols
        return symbols.Take(10);
    }

    public async Task<IEnumerable<AppSymbolData>> GetOhlcvDataAsync(string symbolId, string periodId, string? timeStart,
        string? timeEnd)
    {
        var url = $"https://rest.coinapi.io/v1/ohlcv/{symbolId}/history?period_id={periodId}";
        if (timeStart != null)
        {
            url += $"&time_start={timeStart}";
        }

        if (timeEnd != null)
        {
            url += $"&time_end={timeEnd}";
        }

        var response = await _httpClient.GetAsync(url);
        response.EnsureSuccessStatusCode();

        var responseBody = await response.Content.ReadAsStringAsync();
        var ohlcvData = JsonSerializer.Deserialize<IEnumerable<AppSymbolData>>(responseBody);
        return ohlcvData;
    }
}
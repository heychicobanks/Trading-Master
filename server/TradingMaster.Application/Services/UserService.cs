﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TradingMaster.Application.DTO;
using TradingMaster.Application.Interfaces;
using TradingMaster.Core.Entities;
using TradingMaster.Infrastructure;

namespace TradingMaster.Application.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly TradingMasterContext _dbContext;

        public UserService(UserManager<User> userManager, SignInManager<User> signInManager, TradingMasterContext dbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dbContext = dbContext;
        }

        public async Task<IdentityResult> RegisterUserAsync(AppUserRegistration request)
        {
            var user = new User{ UserName = request.Login, PasswordHash = request.Password};
            return await _userManager.CreateAsync(user, user.PasswordHash);
        }

        public async Task<SignInResult> LoginUserAsync(AppUserLogin request)
        {
            var result =
                await _signInManager.PasswordSignInAsync(request.Login, request.Password, false,
                    lockoutOnFailure: false);
            return result;
        }
        public async Task LogoutUserAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<User> GetUserProfileAsync(string userId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                    return null;
                
                var userProfile = await _dbContext.Users
                    .AsNoTracking() 
                    .FirstOrDefaultAsync(u => u.Id == userId);
                
                return userProfile;                
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error while retrieving user profile", ex);
            }
        }
        
        public async Task<User> GetUserByLoginAsync(string login)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(login);
                return user;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error while retrieving user by login", ex);
            }
        }
        
    }
}

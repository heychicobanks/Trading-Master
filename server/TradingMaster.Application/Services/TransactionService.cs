using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TradingMaster.Application.Interfaces;
using TradingMaster.Core.Entities;
using TradingMaster.Infrastructure;

namespace TradingMaster.Application.Services;

public class TransactionService : ITransactionService
{
    private readonly TradingMasterContext _context;
    private readonly UserManager<User> _userManager;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IMarketService _marketService;

    public TransactionService(
        TradingMasterContext context,
        UserManager<User> userManager,
        IHttpContextAccessor httpContextAccessor,
        IMarketService marketService
    )
    {
        _context = context;
        _userManager = userManager;
        _httpContextAccessor = httpContextAccessor;
        _marketService = marketService;
    }

    public async Task<IEnumerable<Transaction>> GetTransactionsAsync()
    {
        return await _context.Transactions
            .Include(t => t.Wallet)
            .ToListAsync();
    }

    public async Task<Transaction> CreateTransactionAsync(float openingPrice, string assetId)
    {
        var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
        if (user == null) throw new Exception("User not found");

        var userId = user.Id;
        var wallet = await _context.Wallets.FirstOrDefaultAsync(w => w.UserId == userId);
        if (wallet == null) throw new Exception("Wallet not found");

        if (wallet.Balance < openingPrice) throw new Exception("Insufficient funds");

        var transaction = new Transaction
        {
            OpeningPrice = openingPrice,
            Wallet = wallet,
            CreationDate = DateTime.Now,
            Symbol = assetId
        };
        await _context.Transactions.AddAsync(transaction);
        await _context.SaveChangesAsync();
        return transaction;
    }

    public async Task<Transaction> CloseTransactionAsync(Guid transactionId)
    {
        var transaction = await _context.Transactions
            .Include(t => t.Wallet)
            .FirstOrDefaultAsync(t => t.Id == transactionId);

        if (transaction == null) throw new Exception("Transaction not found");

        var assets = await _marketService.GetAssetsAsync(transaction.Symbol);
        var firstAsset = assets.FirstOrDefault();

        if (firstAsset == null) throw new Exception("Asset not found");

        var closingPrice = firstAsset.price_usd;
        transaction.ClosingPrice = closingPrice;
        transaction.ClosingDate = DateTime.Now;
        var profitOrLoss = transaction.ClosingPrice - transaction.OpeningPrice;

        transaction.Wallet.Balance += profitOrLoss.GetValueOrDefault();

        await _context.SaveChangesAsync();
        return transaction;
    }
}
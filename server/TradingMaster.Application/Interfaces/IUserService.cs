﻿using Microsoft.AspNetCore.Identity;
using TradingMaster.Application.DTO;
using TradingMaster.Core.Entities;

namespace TradingMaster.Application.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResult> RegisterUserAsync(AppUserRegistration user);
        Task<SignInResult> LoginUserAsync(AppUserLogin user);
        Task LogoutUserAsync();

        Task<User> GetUserProfileAsync(string userId);

        Task<User> GetUserByLoginAsync(string login);

    }
}
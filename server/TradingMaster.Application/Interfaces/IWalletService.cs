using TradingMaster.Application.DTO;
using TradingMaster.Core.Entities;

namespace TradingMaster.Application.Interfaces
{
    public interface IWalletService
    {
        Task<IEnumerable<Wallet>> GetUserWalletAsync();
    
        Task<Wallet> CreateUserWalletAsync(AppWalletRegistration wallet);

        Task<Wallet> AddBalanceToWalletByWalletId(AppWalletAddBalance request);
        
        Task<Wallet> WithdrawBalanceFromWalletByWalletId(AppWalletWithdrawBalance request);
    }
}


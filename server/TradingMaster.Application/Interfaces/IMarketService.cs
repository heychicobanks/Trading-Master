using TradingMaster.Application.DTO;

namespace TradingMaster.Application.Interfaces;

public interface IMarketService
{
    Task<IEnumerable<AppAsset>> GetAssetsAsync(string? assetId);
    Task<IEnumerable<AppSymbol>> GetSymbolsAsync(string? assetId);

    Task<IEnumerable<AppSymbolData>> GetOhlcvDataAsync(string symbolId, string periodId, string? timeStart,
        string? timeEnd);
}
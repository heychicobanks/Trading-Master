using TradingMaster.Core.Entities;

namespace TradingMaster.Application.Interfaces;

public interface ITransactionService
{
    Task<IEnumerable<Transaction>> GetTransactionsAsync();
    Task<Transaction> CreateTransactionAsync(float openingPrice, string assetId);
    Task<Transaction> CloseTransactionAsync(Guid transactionId);
}
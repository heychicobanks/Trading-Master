﻿namespace TradingMaster.Application.DTO
{
    public class AppUserRegistration
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}

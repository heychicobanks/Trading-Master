namespace TradingMaster.Application.DTO
{
    public class AppWalletRegistration
    {
        public float Balance { get; set; }
        
        public string? UserId { get; set; }
    }
}


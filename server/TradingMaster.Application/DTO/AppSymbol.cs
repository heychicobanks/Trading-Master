namespace TradingMaster.Application.DTO;

public class AppSymbol
{
    public string symbol_id { get; set; }
    public string exchange_id { get; set; }
    public string symbol_type { get; set; }
    public string asset_id_base { get; set; }
    public string asset_id_quote { get; set; }
    public float price { get; set; }
    
}
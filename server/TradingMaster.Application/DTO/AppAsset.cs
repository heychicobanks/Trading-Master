namespace TradingMaster.Application.DTO;

public class AppAsset
{
    public string asset_id { get; set; }
    public string name { get; set; }
    public int type_is_crypto { get; set; }
    public float? price_usd { get; set; }
}
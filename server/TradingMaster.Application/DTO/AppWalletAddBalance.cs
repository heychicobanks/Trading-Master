namespace TradingMaster.Application.DTO
{
  public class AppWalletAddBalance
  {
      public int WalletId {get; set;}
      
      public float Income { get; set; }
  }  
}


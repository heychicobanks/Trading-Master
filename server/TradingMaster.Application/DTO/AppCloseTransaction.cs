namespace TradingMaster.Application.DTO;

public class AppCloseTransaction
{
    public Guid TransactionId { get; set; }
}
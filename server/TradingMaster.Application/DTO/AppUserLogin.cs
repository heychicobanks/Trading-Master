namespace TradingMaster.Application.DTO
{
    public class AppUserLogin
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}

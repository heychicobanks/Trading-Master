namespace TradingMaster.Application.DTO;

public class AppCreateTransaction
{
    public float OpeningPrice { get; set; }
    public string AssetId { get; set; }
}
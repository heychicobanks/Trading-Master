namespace TradingMaster.Application.DTO;

public class AppWalletWithdrawBalance
{
    public int WalletId {get; set;}
    
    public float Withdrawal { get; set; }
}
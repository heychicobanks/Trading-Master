namespace TradingMaster.Application.DTO;

public class AppSymbolData
{
    public string time_period_start { get; set; }
    public string time_period_end { get; set; }
    public string time_open { get; set; }
    public string time_close { get; set; }
    public float price_open { get; set; }
    public float price_high { get; set; }
    public float price_low { get; set; }
    public float price_close { get; set; }
    public float volume_traded { get; set; }
    public int trades_count { get; set; }
}
﻿using Microsoft.EntityFrameworkCore;
using TradingMaster.Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace TradingMaster.Infrastructure
{
    public class TradingMasterContext : IdentityDbContext<IdentityUser,  IdentityRole, string>
    {
        public TradingMasterContext(DbContextOptions<TradingMasterContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TradingMaster.Core.Entities
{
    public class Wallet
    {
        [Key]
        public int Id { get; set; }

        public float Balance { get; set; }
        
        public string UserId { get; set; }
        
        [JsonIgnore]
        public virtual User User { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
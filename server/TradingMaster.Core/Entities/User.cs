﻿using Microsoft.AspNetCore.Identity;

namespace TradingMaster.Core.Entities
{
    public class User : IdentityUser
    {
        public virtual ICollection<Wallet> Wallets { get; set; }
    }
}
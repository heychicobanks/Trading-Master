﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TradingMaster.Core.Entities
{
    public class Transaction
    {
        [Key]
        public Guid Id { get; set; }
        
        [JsonIgnore]
        public Wallet Wallet { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        public DateTime? ClosingDate { get; set; }
        
        [Required]
        public float OpeningPrice { get; set; }
        
        public float? ClosingPrice { get; set; }
        
        [StringLength(10)]
        public string Symbol { get; set; }

        public Transaction()
        {
            CreationDate = DateTime.UtcNow;
        }
    }
}
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TradingMaster.Application.Interfaces;

namespace TradingMaster.Controllers;

[ApiController]
[Route("api/coin-api")]
public class MarketController : Controller
{
    private readonly IMarketService _marketService;
    
    public MarketController(IMarketService marketService)
    {
        _marketService = marketService;
    }
    
    [Authorize]
    [HttpGet("assets")]
    public async Task<IActionResult> GetAssets(string? assetId)
    {
        var assets = await _marketService.GetAssetsAsync(assetId);
        return Ok(assets);
    }
    
    [Authorize]
    [HttpGet("symbols")]
    public async Task<IActionResult> GetSymbols(string? assetId)
    {
        var symbols = await _marketService.GetSymbolsAsync(assetId);
        return Ok(symbols);
    }
    
    [Authorize]
    [HttpGet("ohlcv-data")]
    public async Task<IActionResult> GetOhlcvData(string symbolId, string periodId, string? timeStart, string? timeEnd)
    {
        var ohlcvData = await _marketService.GetOhlcvDataAsync(symbolId, periodId, timeStart, timeEnd);
        return Ok(ohlcvData);
    }
}
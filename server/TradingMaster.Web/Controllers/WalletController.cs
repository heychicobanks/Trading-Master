using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TradingMaster.Application.DTO;
using TradingMaster.Application.Interfaces;

namespace TradingMaster.Controllers
{
    [ApiController]
    [Route("api/wallets")]
    public class WalletController : ControllerBase
    {
        private readonly IWalletService _walletService;

        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }

        [Authorize]
        [HttpPost("addWallet")]
        public async Task<IActionResult> AddWallet([FromBody] AppWalletRegistration wallet)
        {
            var result = await _walletService.CreateUserWalletAsync(wallet);

            return Ok("Wallet created successfully : " + result);
        }

        [Authorize]
        [HttpGet("userWallets")]
        public async Task<IActionResult> GetWallets()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
                return Unauthorized();

            var userWallets = await _walletService.GetUserWalletAsync();

            return Ok(userWallets);
        }

        [Authorize]
        [HttpPost("addBalance")]
        public async Task<IActionResult> AddBalanceToWalletByWalletId([FromBody] AppWalletAddBalance request)
        {
            var updatedWallet = await _walletService.AddBalanceToWalletByWalletId(request);

            return Ok(updatedWallet);
        }

        [Authorize]
        [HttpPatch("withdrawBalance")]
        public async Task<IActionResult> WithdrawBalanceFromWalletByUserIdAndWalletId(
            [FromBody] AppWalletWithdrawBalance request)
        {
            var updatedWallet = await _walletService.WithdrawBalanceFromWalletByWalletId(request);

            return Ok(updatedWallet);
        }
    }
}
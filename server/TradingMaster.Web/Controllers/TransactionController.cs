using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TradingMaster.Application.DTO;
using TradingMaster.Application.Interfaces;

namespace TradingMaster.Controllers;

[ApiController]
[Route("api/transactions")]
public class TransactionController : Controller
{
    private readonly ITransactionService _transactionService;

    public TransactionController(ITransactionService transactionService)
    {
        _transactionService = transactionService;
    }

    [Authorize]
    [HttpGet]
    public async Task<IActionResult> GetTransactions()
    {
        var transactions = await _transactionService.GetTransactionsAsync();
        return Ok(transactions);
    }

    [Authorize]
    [HttpPost]
    public async Task<IActionResult> CreateTransaction([FromBody] AppCreateTransaction input)
    {
        var transaction = await _transactionService.CreateTransactionAsync(input.OpeningPrice, input.AssetId);
        return Ok(transaction);
    }

    [Authorize]
    [HttpPost("close")]
    public async Task<IActionResult> CloseTransaction([FromBody] AppCloseTransaction input)
    {
        var transaction = await _transactionService.CloseTransactionAsync(input.TransactionId);
        return Ok(transaction);
    }
}
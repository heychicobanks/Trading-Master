using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TradingMaster.Application.DTO;
using TradingMaster.Application.Interfaces;

namespace TradingMaster.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IWalletService _walletService;

        public UsersController(IUserService userService, IWalletService walletService)
        {
            _userService = userService;
            _walletService = walletService;
        }

        [HttpPost("signup")]
        public async Task<IActionResult> RegisterUser([FromBody] AppUserRegistration user)
        {
            if (user == null || !ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _userService.RegisterUserAsync(user);

            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, result.Errors);

            var createdUser = await _userService.GetUserByLoginAsync(user.Login);

            if (createdUser == null)
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "User registration succeeded, but unable to retrieve created user.");

            var wallet = new AppWalletRegistration()
            {
                Balance = 0,
                UserId = createdUser.Id
            };

            var walletResult = await _walletService.CreateUserWalletAsync(wallet);

            return Ok("User created successfully!" + walletResult);
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginUser([FromBody] AppUserLogin user)
        {
            if (user == null || !ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _userService.LoginUserAsync(user);
            if (!result.Succeeded)
                return Unauthorized();

            return Ok("User logged in successfully!");
        }

        [Authorize]
        [HttpPost("logout")]
        public async Task<IActionResult> LogoutUser()
        {
            await _userService.LogoutUserAsync();
            return Ok("User logged out successfully!");
        }

        [Authorize]
        [HttpGet("profile")]
        public async Task<IActionResult> GetUserProfile()
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                if (string.IsNullOrEmpty(userId))
                    return Unauthorized();

                var userProfile = await _userService.GetUserProfileAsync(userId);
                if (userProfile == null)
                    return NotFound();

                return Ok(userProfile);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while fetching user profile: {ex.Message}");
            }
        }
    }
}
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Json.Serialization;
using TradingMaster.Infrastructure;
using TradingMaster.Application.Interfaces;
using TradingMaster.Application.Services;
using TradingMaster.Core.Entities;

var builder = WebApplication.CreateBuilder(args);

#region "R�cup�ration du fichier de configuration"

// R�cup�ration du fichier de configuration
var configurationBuilder = new ConfigurationBuilder();

configurationBuilder.SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

IConfiguration config = configurationBuilder.Build();

#endregion

#region "Connection à la base de données"
string connectionString = config["ConnectionStrings:TradingDatabase"] ?? throw new Exception("Database connection string not found");

// Connection � la base de donn�es
builder.Services.AddDbContext<TradingMasterContext>(options =>
    options.UseSqlServer(connectionString, b => b.MigrationsAssembly("TradingMaster.Infrastructure")));

#endregion

#region "Configuration de l'architecture du serveur WEB"

#region "JWT"

//Mise en place de la s�curisation des routes JWT
string secret = config["JWT:secret"] ?? throw new Exception("Cl� secrete JWT non trouv�e");

var key = Encoding.ASCII.GetBytes(secret);

builder.Services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(x =>
    {
        x.RequireHttpsMetadata = false;
        x.SaveToken = true;
        x.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false
        };
    });

#endregion

// D�finition des controllers
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
});

// D�finition des services 
builder.Services.AddHttpContextAccessor();
builder.Services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<TradingMasterContext>()
    .AddDefaultTokenProviders();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IWalletService, WalletService>();
builder.Services.AddScoped<IMarketService, MarketService>();
builder.Services.AddScoped<ITransactionService, TransactionService>();
builder.Services.AddHttpClient();

#endregion

#region "Ne pas toucher"

// Swagger
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();
app.MapControllers();
app.Run();

#endregion
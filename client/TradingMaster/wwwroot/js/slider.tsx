﻿document.addEventListener("DOMContentLoaded", function () {
    initializeSlider();
});

function initializeSlider() {
    let slideIndex = 0;
    const slides = document.getElementsByClassName("slide");
    const indicators = document.getElementsByClassName("indicator");

    if (slides.length === 0 || indicators.length === 0) {
        console.error("Slides or indicators not found");
        return;
    }

    showSlides(slideIndex);

    for (let i = 0; i < indicators.length; i++) {
        indicators[i].addEventListener("click", function () {
            slideIndex = i;
            updateSlides();
        });
    }

    function showSlides(n) {
        for (let i = 0; i < slides.length; i++) {
            slides[i].classList.remove("active");
            indicators[i].classList.remove("active");
        }
        slides[slideIndex].classList.add("active");
        indicators[slideIndex].classList.add("active");
        setTimeout(function () {
            slideIndex = (slideIndex + 1) % slides.length;
            showSlides(slideIndex);
        }, 5000);
    }

    function updateSlides() {
        for (let i = 0; i < slides.length; i++) {
            slides[i].classList.remove("active");
            indicators[i].classList.remove("active");
        }
        slides[slideIndex].classList.add("active");
        indicators[slideIndex].classList.add("active");
    }
}
